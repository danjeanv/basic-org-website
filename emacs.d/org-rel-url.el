; from https://emacs.stackexchange.com/questions/9807/org-mode-dont-change-relative-urls

(require 'org)

(defun org-rel-url-export (path desc format)
  (cl-case format
    (html (format "<a href=\"%s\">%s</a>" path (or desc path)))
    (latex (format "\href{%s}{%s}" path (or desc path)))
    (otherwise path)))

(eval-after-load "org"
  '(org-link-set-parameters "rel" :follow #'browse-url :export #'org-rel-url-export))

(provide 'org-rel-url)
